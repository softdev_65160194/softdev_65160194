/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab1;

/**
 *
 * @author wittaya5329
 */
import java.util.Scanner;

public class tictactoe {
    static char[][] table;
    
    
    public static void main(String[] args) {
        System.out.println("Welcome to tic tac toe");
        table = new char[4][4];
        Scanner kb = new Scanner(System.in);
        for(int i =1;i <4;i++){
            for(int j =1;j <4;j++){
                table[i][j] = '-';
            }
        }
        boolean p1 = true;

        boolean gameend = false;

        while (!gameend) {
            printtable(table);
            char turn = ' ';
            if(p1){
                turn = 'X';
            }else{
                turn = 'O';
            }
            System.out.println(turn +" turn");

            int row = 0;
            int col = 0;
            while (true) {
                System.out.println("enter a row");
                row = kb.nextInt();
                System.out.println("enter a col");
                col = kb.nextInt();

                if(row < 1||row > 3||col < 1||col > 3){
                    System.out.println("invalid row or column");
                } else if(table[row][col] != '-'){
                    System.out.println("this row or col have already taken");
                }else{
                    break;
                }
            }
            table[row][col] = turn;
            if(winner(table) == 'X'){
                printtable(table);
                System.out.println(turn +" has won");
                gameend = true;
            }else if(winner(table) == 'O'){
                printtable(table);
                System.out.println(turn +" has won");
                gameend = true;
            }else{
                if(draw(table)){
                    printtable(table);
                    System.out.println("it draw");
                    gameend = true;
                }
                else{
                    p1 = !p1;
                }
            }
        }  
    }
    public static void printtable(char[][] table2){
        for(int i =1;i <4;i++){
            for(int j =1;j <4;j++){
                System.out.print(table[i][j]+" ");
            }
            System.out.println();
        }
    }
    static char winner(char[][] table){
        for(int i =1;i <4;i++){
            if(table[i][1] == table[i][2] && table[i][2] == table[i][3] && table[i][1] != '-'){
                return table[i][1];
            }
        }
        for(int j =1;j <4;j++){
            if(table[1][j] == table[2][j] && table[2][j] == table[3][j] && table[1][j] != '-'){
                return table[1][j];
            }
        }
        
        if(table[1][1] == table[2][2] && table[2][2] == table[3][3] && table[1][1] != '-'){
                return table[1][1];
            
        }
        
        if(table[1][3] == table[2][2] && table[2][2] == table[3][1] && table[1][3] != '-'){
                return table[1][3];
        }
        return '-';
    }
    static boolean draw(char[][] table){
        for(int i =1;i <4;i++){
            for(int j =1;j <4;j++){
                if(table[i][j]=='-'){
                    return false;
                }
            }
        }
        return true;
    }
}


